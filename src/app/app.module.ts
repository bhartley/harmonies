import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { RouterModule } from "@angular/router";
import { routes } from "./routes";

import { AppComponent } from "./app.component";
import { ProgressionsComponent } from "./components/progressions/progressions.component";
import { ScalesComponent } from "./components/scales/scales.component";

import { NoteToStringPipe } from "./notes/NoteToString.pipe";

@NgModule({
  declarations: [
    AppComponent,
    ProgressionsComponent,
    ScalesComponent,
    NoteToStringPipe,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
      // { enableTracing: true }
    )
  ],
  providers: [],
  bootstrap: [
    AppComponent,
    ProgressionsComponent,
    ScalesComponent,
  ]
})
export class AppModule { }
