import { HALF_STEP as H, WHOLE_STEP as W } from "../constants/steps";

export const SCALES = {
  HARMONIC_MINOR: [W, H, W, W, H, H + W, H],
  MAJOR: [W, W, H, W, W, W, H],
  NATURAL_MINOR: [W, H, W, W, H, W, W],
};

export enum SCALE_NAMES {
  ALL = "all notes",
  HARMONIC_MINOR = "harmonic minor",
  MAJOR = "major",
  MINOR = "minor",
  NATURAL_MINOR = "natural minor",
}
