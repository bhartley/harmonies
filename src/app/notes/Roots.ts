import { NoteList } from "./NoteList";
import { NOTES } from "./NOTES";

export class Roots {
  public [NOTES.A]: NoteList;
  public [NOTES.Bb]: NoteList;
  public [NOTES.B]: NoteList;
  public [NOTES.C]: NoteList;
  public [NOTES.Db]: NoteList;
  public [NOTES.D]: NoteList;
  public [NOTES.Eb]: NoteList;
  public [NOTES.E]: NoteList;
  public [NOTES.F]: NoteList;
  public [NOTES.Gb]: NoteList;
  public [NOTES.G]: NoteList;
  public [NOTES.Ab]: NoteList;

  constructor() {
    this[NOTES.A] = new NoteList(NOTES.A);
    this[NOTES.Bb] = new NoteList(NOTES.Bb);
    this[NOTES.B] = new NoteList(NOTES.B);
    this[NOTES.C] = new NoteList(NOTES.C);
    this[NOTES.Db] = new NoteList(NOTES.Db);
    this[NOTES.D] = new NoteList(NOTES.D);
    this[NOTES.Eb] = new NoteList(NOTES.Eb);
    this[NOTES.E] = new NoteList(NOTES.E);
    this[NOTES.F] = new NoteList(NOTES.F);
    this[NOTES.Gb] = new NoteList(NOTES.Gb);
    this[NOTES.G] = new NoteList(NOTES.G);
    this[NOTES.Ab] = new NoteList(NOTES.Ab);
  }

  public static getAllNotes(): any {
    const notes: any[] = [];
    for (const note in NOTES) {
      if (isNaN(+note)) {
        notes.push(note);
      }
    }
    return notes;
  }
}
