import { INTERVAL } from "./INTERVAL";
import { NOTES } from "./NOTES";

export function intervalToString(i: INTERVAL) {
  switch (i) {
    case INTERVAL.ROOT:
      return "root";
    case INTERVAL.FLAT_NINTH:
    case INTERVAL.FLAT_SECOND:
    case INTERVAL.MINOR_SECOND:
      return "flat second";
    case INTERVAL.MAJOR_NINTH:
    case INTERVAL.MAJOR_SECOND:
      return "major second";
    case INTERVAL.MINOR_THIRD:
      return "minor third";
    case INTERVAL.MAJOR_THIRD:
      return "major third";
    case INTERVAL.PERFECT_FOURTH:
      return "perfect fourth";
    case INTERVAL.TRITONE:
    case INTERVAL.FLAT_FIFTH:
      return "flat fifth";
    case INTERVAL.PERFECT_FIFTH:
      return "perfect fifth";
    case INTERVAL.SHARP_FIFTH:
    case INTERVAL.FLAT_SIXTH:
    case INTERVAL.FLAT_THIRTEENTH:
      return "sharp fifth";
    case INTERVAL.SIXTH:
    case INTERVAL.MAJOR_SIXTH:
    case INTERVAL.THIRTEENTH:
    case INTERVAL.MAJOR_THIRTEENTH:
      return "major sixth";
    case INTERVAL.DOMINANT_SEVENTH:
    case INTERVAL.FLAT_SEVENTH:
      return "flat seventh";
    case INTERVAL.MAJOR_SEVENTH:
      return "major seventh";
    default:
  }
}

export function stringToNote(str: string): NOTES {
  switch (str) {
    case "A":
      return NOTES.A;
    case "A#":
    case "Bb":
      return NOTES.Bb;
    case "B":
      return NOTES.B;
    case "C":
      return NOTES.C;
    case "C#":
    case "Db":
      return NOTES.Db;
    case "D":
      return NOTES.D;
    case "D#":
    case "Eb":
      return NOTES.Eb;
    case "E":
      return NOTES.E;
    case "F":
      return NOTES.F;
    case "F#":
    case "Gb":
      return NOTES.Gb;
    case "G":
      return NOTES.G;
    case "G#":
    case "Ab":
      return NOTES.Ab;
    default:
      return null;
  }
}

export function noteToString(note: NOTES, sharps: boolean = true): string {
  switch (note) {
    case NOTES.A:
      return "A";
    case NOTES.Bb:
      return sharps ? "A#" : "Bb";
    case NOTES.B:
      return "B";
    case NOTES.C:
      return "C";
    case NOTES.Db:
      return sharps ? "C#" : "Db";
    case NOTES.D:
      return "D";
    case NOTES.Eb:
      return sharps ? "D#" : "Eb";
    case NOTES.E:
      return "E";
    case NOTES.F:
      return "F";
    case NOTES.Gb:
      return sharps ? "F#" : "Gb";
    case NOTES.G:
      return "G";
    case NOTES.Ab:
      return sharps ? "G#" : "Ab";
    default:
      return "";
  }
}

export function sharpToFlat(note: string) {
  let tmp = note;
  switch (note) {
    case "A#":
      tmp = "Bb";
      break;
    case "C#":
      tmp = "Db";
      break;
    case "D#":
      tmp = "Eb";
      break;
    case "F#":
      tmp = "Gb";
      break;
    case "G#":
      tmp = "Ab";
      break;
    default:
      break;
  }
  return tmp;
}

export function flatToSharp(note: string) {
  let tmp = note;
  switch (note) {
    case "Ab":
      tmp = "G#";
      break;
    case "Bb":
      tmp = "A#";
      break;
    case "Db":
      tmp = "C#";
      break;
    case "Eb":
      tmp = "D#";
      break;
    case "Gb":
      tmp = "F#";
      break;
    default:
      break;
  }
  return tmp;
}
