import { CircularLinkedList } from "ts-implementations";

import { flatToSharp, noteToString, sharpToFlat, stringToNote } from "./utils";

import { NOTES } from "./NOTES";
const TWO_OCTAVES = [
  NOTES.A, NOTES.Bb, NOTES.B, NOTES.C, NOTES.Db, NOTES.D, NOTES.Eb, NOTES.E, NOTES.F, NOTES.Gb, NOTES.G, NOTES.Ab,
  NOTES.A, NOTES.Bb, NOTES.B, NOTES.C, NOTES.Db, NOTES.D, NOTES.Eb, NOTES.E, NOTES.F, NOTES.Gb, NOTES.G, NOTES.Ab,
];

import { SCALES, SCALE_NAMES } from "../scales";

export class NoteList extends CircularLinkedList {
  public root: NOTES;

  constructor(root: NOTES) {
    super();
    this.root = root;
    const idx = TWO_OCTAVES.indexOf(root);
    for (let i = idx; i < idx + 12; i++) {
      this.pushEnd(TWO_OCTAVES[i]);
    }
  }

  public getList(sharps = true) {
    let node = this.getHead();
    const tmp: string[] = [];
    for (let i = 0, len = this.getLength(); i < len; i++) {
      const note = noteToString(node.getData());
      tmp.push(sharps ? flatToSharp(note) : sharpToFlat(note));
      node = node.getNext();
    }
    return tmp;
  }

  public getHarmonicMinorScaleNotes() {
    return this.handleScalePattern(SCALES.HARMONIC_MINOR.slice());
  }
  public getHarmonicMinorScaleString(sharps = true) {
    return this.handleScalePatternString(SCALES.HARMONIC_MINOR.slice(), sharps);
  }

  public getMajorScaleNotes() {
    return this.handleScalePattern(SCALES.MAJOR.slice());
  }
  public getMajorScaleString(sharps = true) {
    return this.handleScalePatternString(SCALES.MAJOR.slice(), sharps);
  }

  public getMinorScaleNotes() {
    return this.handleScalePattern(SCALES.NATURAL_MINOR.slice());
  }
  public getMinorScaleString(sharps = true) {
    return this.handleScalePatternString(SCALES.NATURAL_MINOR.slice(), sharps);
  }
  public getNaturalMinorScaleNotes() {
    return this.getMinorScaleNotes();
  }
  public getNaturalMinorScaleString(sharps = true) {
    return this.getMinorScaleString(sharps);
  }

  public getScaleFromScaleName(scaleName: SCALE_NAMES, sharps = true) {
    switch (scaleName) {
      case SCALE_NAMES.ALL:
        return this.getList(sharps);
      case SCALE_NAMES.MAJOR:
        return this.getMajorScaleString(sharps);
      case SCALE_NAMES.HARMONIC_MINOR:
        return this.getHarmonicMinorScaleString(sharps);
      case SCALE_NAMES.MINOR:
      case SCALE_NAMES.NATURAL_MINOR:
        return this.getNaturalMinorScaleString(sharps);
      default:
    }
  }

  private handleScalePattern(pattern: number[]) {
    let node = this.getHead();
    const tmp: NOTES[] = [];

    // add root note
    tmp.push(node.getData());

    // loop through intervals and add notes from list
    while (pattern[0]) {
      const step = pattern.shift();
      for (let i = 0; i < step; i++) {
        node = node.getNext();
      }
      tmp.push(node.getData());
    }

    return tmp;
  }

  private handleScalePatternString(pattern: number[], sharps = true) {
    let node = this.getHead();
    const tmp: string[] = [];

    // add root note
    const root = noteToString(node.getData());
    tmp.push(sharps ? flatToSharp(root) : sharpToFlat(root));

    // loop through intervals and add notes from list
    while (pattern[0]) {
      const step = pattern.shift();
      for (let i = 0; i < step; i++) {
        node = node.getNext();
      }
      const note = noteToString(node.getData());
      tmp.push(sharps ? flatToSharp(note) : sharpToFlat(note));
    }

    return tmp;
  }

}
