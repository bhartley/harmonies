import { CircularLinkedList } from "ts-implementations";

import { INTERVAL } from "./INTERVAL";

export class IntervalList extends CircularLinkedList {

  constructor() {
    super();
    this.pushEnd(INTERVAL.ROOT);
    this.pushEnd(INTERVAL.FLAT_SECOND);
    this.pushEnd(INTERVAL.MAJOR_SECOND);
    this.pushEnd(INTERVAL.MINOR_THIRD);
    this.pushEnd(INTERVAL.MAJOR_THIRD);
    this.pushEnd(INTERVAL.PERFECT_FOURTH);
    this.pushEnd(INTERVAL.FLAT_FIFTH);
    this.pushEnd(INTERVAL.PERFECT_FIFTH);
    this.pushEnd(INTERVAL.FLAT_SIXTH);
    this.pushEnd(INTERVAL.MAJOR_SIXTH);
    this.pushEnd(INTERVAL.FLAT_SEVENTH);
    this.pushEnd(INTERVAL.MAJOR_SEVENTH);
  }

}
