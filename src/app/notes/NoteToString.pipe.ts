import { Pipe, PipeTransform } from "@angular/core";
import { NOTES } from "./NOTES";
import { noteToString } from "./utils";

@Pipe({ name: "NoteToString" })
export class NoteToStringPipe implements PipeTransform {
  transform(val: NOTES[] | NOTES): string[] {
    if (Array.isArray(val)) {
      const tmp: string[] = [];
      for (const v of val) {
        tmp.push(noteToString(v));
      }
      return tmp;
    }
    return [ noteToString(val) ];
  }
}
