import { Component, OnInit } from "@angular/core";

import { Chord, Progression } from "../../chords";
import { flatToSharp, NOTES, noteToString, sharpToFlat, stringToNote } from "../../notes";
import { MajorKeyChords } from "../../chords/MajorKeyChords";

const DEFAULT_KEY = NOTES.C;

@Component({
  selector: "app-progressions",
  templateUrl: "./progressions.component.html",
  styleUrls: [
    "./progressions.component.css",
  ]
})
export class ProgressionsComponent implements OnInit {
  _key: NOTES;
  key: string;
  prog: Progression;
  progChords: Chord[] = [];
  progString: string[] = [];

  majorChords: MajorKeyChords;

  ngOnInit() {
    this.setKey(DEFAULT_KEY);
    this.majorChords = new MajorKeyChords(this._key);
    this.defaultProgression();
  }

  public setKey(key: NOTES | string) {
    if (typeof(key) === "string") {
      this._key = stringToNote(key);
    } else {
      this._key = key;
    }
    this.prog = new Progression(DEFAULT_KEY);
  }

  private defaultProgression() {
    this.clearProgression();
    this.prog.pushEnd(this.majorChords.get("I"));
    this.prog.pushEnd(this.majorChords.get("iii"));
    this.prog.pushEnd(this.majorChords.get("IV"));
    this.prog.pushEnd(this.majorChords.get("V"));
    this.progChords = this.prog.getList();
    this.progString = this.prog.getChordsString();
  }

  private clearProgression() {
    while (this.prog.getHead().getData() !== null) {
      this.prog.deleteFirst();
    }
  }

}
