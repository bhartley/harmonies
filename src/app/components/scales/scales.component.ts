import { Component, OnInit } from "@angular/core";

import { Chord, Progression } from "../../chords";
import { flatToSharp, NOTES, noteToString, Roots, sharpToFlat, stringToNote } from "../../notes";
import { SCALE_NAMES } from "../../scales";

const roots = new Roots();

const DEFAULT_KEY = NOTES.C;

@Component({
  selector: "app-scales",
  templateUrl: "./scales.component.html",
  styleUrls: [
    "./scales.component.css",
  ]
})
export class ScalesComponent implements OnInit {
  private _key: NOTES;
  public key: string;
  public sharps = true;

  public notelist: string[] = [];
  public notelistName: SCALE_NAMES;

  ngOnInit() {
    this.notelistName = SCALE_NAMES.ALL;
    this.setKey(DEFAULT_KEY);
  }

  public setKey(key: NOTES | string) {
    if (typeof(key) === "string") {
      // this.sharps = !!["A", "A#", "B", "C", "C#", "D", "D#", "E", "F#", "G", "G#"].includes(key);
      this._key = stringToNote(key);
    } else {
      this._key = key;
      // const tmp = noteToString(key);
      // this.sharps = !!["A", "A#", "B", "C", "C#", "D", "D#", "E", "F#", "G", "G#"].includes(tmp);
      // this._key = stringToNote(tmp);
    }
    this.key = noteToString(this._key, this.sharps);

    this.notelist = roots[this._key].getScaleFromScaleName(this.notelistName, this.sharps);
  }

  public getAllKeys(): any {
    const keys: any[] = [];
    for (const key of Roots.getAllNotes()) {
      keys.push(this.sharps ? flatToSharp(key) : sharpToFlat(key));
    }
    return keys;
  }

  public toggleSharps(): void {
    this.sharps = !this.sharps;
    this.key = this.sharps ? flatToSharp(this.key) : sharpToFlat(this.key);

    const tmp: string[] = [];
    for (const note of this.notelist) {
      tmp.push(this.sharps ? flatToSharp(note) : sharpToFlat(note));
    }
    this.notelist = tmp;
  }

  public allNotes() {
    this.notelist = roots[this._key].getList(this.sharps);
    this.notelistName = SCALE_NAMES.ALL;
  }
  public harmonicMinorScale() {
    this.notelist = roots[this._key].getHarmonicMinorScaleString(this.sharps);
    this.notelistName = SCALE_NAMES.HARMONIC_MINOR;
  }
  public majorScale() {
    this.notelist = roots[this._key].getMajorScaleString(this.sharps);
    this.notelistName = SCALE_NAMES.MAJOR;
  }
  public minorScale() {
    this.notelist = roots[this._key].getMinorScaleString(this.sharps);
    this.notelistName = SCALE_NAMES.MINOR;
  }
  public naturalMinorScale() {
    this.minorScale();
  }

}
