import { Routes } from "@angular/router";
import { ScalesComponent } from "./components/scales/scales.component";
import { ProgressionsComponent } from "./components/progressions/progressions.component";

export const routes: Routes = [
  { path: "scales", component: ScalesComponent },
  { path: "progressions", component: ProgressionsComponent },
  { path: "**", component: ScalesComponent}
];
