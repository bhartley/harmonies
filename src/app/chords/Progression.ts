import { CircularLinkedList } from "ts-implementations";

import { Chord } from "./Chord"; // todo: enforce only pushing this type on to the list

import { NOTES, stringToNote } from "../notes";

export class Progression extends CircularLinkedList {
  public key: NOTES;

  constructor(key: NOTES | string) {
    super();

    if (typeof(key) === "string") {
      this.key = stringToNote(key);
    } else {
      this.key = key;
    }
  }

  public getChordsString() {
    const tmp: any[] = [];
    for (const chord of this.getList()) {
      tmp.push(chord.getNotesString());
    }
    return tmp;
  }

}
