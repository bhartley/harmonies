import { CircularLinkedList } from "ts-implementations";

import { NoteList, NOTES, stringToNote } from "../notes";
import { noteToString } from "../notes/utils";

export class Chord extends CircularLinkedList {
  public name = "";
  private noteList: NoteList;
  public root: NOTES;

  constructor(root: NOTES | string) {
    super();

    if (typeof(root) === "string") {
      this.root = stringToNote(root);
    } else {
      this.root = root;
    }
    this.noteList = new NoteList(this.root);
  }

  public major(): void {
    this.clearNotes();
    this.name = "major";

    let node = this.noteList.getHead();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext().getNext();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext();
    this.pushEnd(node.getData());
  }

  public minor(): void {
    this.clearNotes();
    this.name = "minor";

    let node = this.noteList.getHead();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext().getNext();
    this.pushEnd(node.getData());
  }

  public diminished(): void {
    this.clearNotes();
    this.name = "dim";

    let node = this.noteList.getHead();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext();
    this.pushEnd(node.getData());
    node = node.getNext().getNext().getNext();
    this.pushEnd(node.getData());
  }

  public getNotes(): NOTES[] {
    return this.getList();
  }

  public getNotesString(): string[] {
    const tmp: string[] = [];
    const notes = this.getList();
    for (const note of notes) {
      tmp.push(noteToString(note));
    }
    return tmp;
  }

  private clearNotes(): void {
    while (this.getHead().getData() !== null) {
      this.deleteFirst();
    }
    this.name = "";
  }

}
