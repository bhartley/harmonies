import { Chord } from "./Chord";
import { NOTES, Roots, stringToNote } from "../notes";
const roots = new Roots();

export class MajorKeyChords extends Map<string, Chord> {
  private majorScale: NOTES[] = [];
  public key: NOTES;

  constructor(key: NOTES | string) {
    super();
    if (typeof(key) === "string") {
      this.key = stringToNote(key);
    } else {
      this.key = key;
    }
    this.majorScale = roots[this.key].getMajorScaleNotes();

    const I = new Chord(this.majorScale[0]);
    I.major();
    this.set("I", I);

    const ii = new Chord(this.majorScale[1]);
    ii.minor();
    this.set("ii", ii);

    const iii = new Chord(this.majorScale[2]);
    iii.minor();
    this.set("iii", iii);

    const IV = new Chord(this.majorScale[3]);
    IV.major();
    this.set("IV", IV);

    const V = new Chord(this.majorScale[4]);
    V.major();
    this.set("V", V);

    const vi = new Chord(this.majorScale[5]);
    vi.minor();
    this.set("vi", vi);

    const viio = new Chord(this.majorScale[6]);
    viio.diminished();
    this.set("viio", viio);
  }

}
